package com.arisa.week11;

public class Rat extends Animal implements Walkable {
    public Rat(String name){
        super(name, 4);
    }

    @Override
    public void Walk() {
        System.out.println(this.toString() + " walk.");
        
    }

    @Override
    public void Run() {
        System.out.println(this.toString() + " run.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
        
    }
    @Override
    public String toString() {
        return "Rat(" + this.getName() + ")";
    }
}
