package com.arisa.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.Swim();

        Plane plane1 = new Plane("Boeing", "Boeing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Crocodile croc1 = new Crocodile("Crocky");
        croc1.eat();
        croc1.sleep();
        croc1.Swim();
        croc1.crawl();

        Snake snake1 = new Snake("Zally");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();

        Submarine sub1 = new Submarine("Marine", "Marine Engine");
        sub1.Swim();

        Bird bird1 = new Bird("Angry");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Cat cat1 = new Cat("Ming");
        cat1.eat();
        cat1.sleep();
        cat1.Walk();
        cat1.Run();

        Dog dog1 = new Dog("Tam");
        dog1.eat();
        dog1.sleep();
        dog1.Walk();
        dog1.Run();

        Human hu1 = new Human("Arthy");
        hu1.eat();
        hu1.sleep();
        hu1.Walk();
        hu1.Run();

        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.sleep();
        rat1.Walk();
        rat1.Run();

        Flyable[] flyableObjects = {bat1,plane1,bird1};
        for(int i=0; i<flyableObjects.length;i++){
            flyableObjects[i].takeoff();
            flyableObjects[i].fly();
            flyableObjects[i].landing();
        }

        Walkable[] WalkableObjects = {rat1,cat1,dog1,hu1};
        for(int i=0; i<WalkableObjects.length;i++){
            WalkableObjects[i].Walk();
            WalkableObjects[i].Run();
        }

        Swimable[] SwimableObjects = {fish1,sub1,croc1};
        for(int i=0; i<SwimableObjects.length;i++){
            SwimableObjects[i].Swim();
        }

        Crawlable[] CrawlableObjects = {snake1,croc1};
        for(int i=0; i<CrawlableObjects.length;i++){
            CrawlableObjects[i].crawl();
        }
    }
}
