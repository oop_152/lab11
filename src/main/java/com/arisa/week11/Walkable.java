package com.arisa.week11;

public interface Walkable {
    public void Walk();
    public void Run();
}
